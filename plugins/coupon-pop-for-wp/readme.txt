﻿=== Automatic Lead Generator for WooCommerce ===
Contributors: StoreYa
Tags: Facebook, ecommerce, import, Viral marketing, woocommerce, woo-commerce, wp-commerce, wp, Rewards, Facebook marketing, SocialMarketing, Online Sales, Marketing tools, Social commerce, storeya, Promotional tool, engagement, Online  marketing, EmailMarketing, Twitter, G+, google, google plus, Instagram, Pinterest, LinkedIn, likes, get likes, buy likes, fans, add fans, buyfans, Incentive app, Social campaigns, ads, seo, ppc, wordpress, share, blog, plugin, sharing, network, tumblr, coupon, email, facebook likes, get likes, increase sales, likes, twitter followers, deals, discount, sales, widget, token,  paypal, leads, lead generator, lead generation
Requires at least: 3.0
Tested up to: 3.8
Stable tag: 2.1
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Automatic Lead Generator plugin increasing your visitors' engagement and conversion rate from day one!

== Description ==

**Exposed to 10 million customers every month, the Coupon Pop results talk for themselves:**

- Boost sales by 20%

- Grow Facebook communities by 25%

- Increase Web store’s pageviews by 250%

- Generates thousands of email addresses for your future sales

Easy 2 minutes installation – free for Beta!

The Coupon Pop enables you to pre-define a promotion that would pop up to your visitors asking them to join you in any social network you are active or wish to be active on as well as your to your emails list. Coupon pop works with Facebook, Twitter, Google+, LinkedIn.

The Coupon Pop Plugin is designed to boost your web store’s conversion rate and to increases your fan base and email lists by popping up special offers and discounts to your visitors.

To learn more about StoreYa and how it works visit [our website](http://www.storeya.com/public/couponpop).

== Installation ==

1. Download the  zip file and save it locally - 'storeya-coupon-pop.zip'.

2. Log in to your WordPress administration screen.

3. Click on 'Plugins' and then on 'Add New' (left menu).

4. Click on the 'Upload' link, choose 'storeya-coupon-pop.zip' file and click on the 'Install' now button.

5. Click on 'Activate Plugin' link

6. Click on the StoreYa Coupon Pop > Settings

8. Go to www.storeya.com/public/couponpop

9. If you are not logged in, please click on the "Create a Free Coupon Pop Now!" button.

10. Once you are happy with your Coupon Pop customization, copy the code and paste it to the Coupon Pop for WP setting screen. 

11. Click 'Save changes'.

== Screenshots ==

1. Coupon Pop is compatible to Desktop, Tablet, and mobile.

2. A live Coupon Pop example - 2.

3. A live Coupon Pop example - 3.

4. A live Coupon Pop example - 4.

5. A live Coupon Pop example - 5.

6. A live Coupon Pop example - 6.

7. A live Coupon Pop example - 7.

== Resources ==

[StoreYa support](https://storeya.zendesk.com/home) 

[Coupon Pop website](http://www.storeya.com/public/couponpop)  

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
