jQuery(document).ready(function($) {
  $('.dymo-link').click(function (event){
    var url = $(this).attr("href");
    if ($.browser.webkit) {
      window.open(url, "Print", "width=500, height=500");
    }
    else {
      window.open(url, "Print", "scrollbars=1, width=500, height=500");
    }
    
    event.preventDefault();
	
    return false;
 
  });
});

jQuery(document).on( 'click', '.dymo-update-notice .notice-dismiss', function() {
    jQuery.ajax({
        url: ajaxurl,
        data: {
            action: 'dymo_dismiss_update_notice'
        }
    })

})