<?php get_template_part('templates/page', 'header'); ?>

<h2><?php _e('Ooops! ','agp2015') ?></h2>
<p><?php _e('Sorry, but it looks like we can\'t find what you\'re looking for.','startertheme') ?></p>
<img src="<?php echo get_template_directory_uri(); ?>/dist/images/search.png" alt="search" class="pull-right"/>
