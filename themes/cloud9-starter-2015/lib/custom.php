function bootstapHidden() {
	// Create empty array to store in to
	$notVisibleOn = array();

	// if not wanted to display on Large devices
	if( !in_array( 'col-lg', get_sub_field('element_visible_on') ) ) {
		array_push($notVisibleOn, 'hidden-lg');
		$lgHidden = true;
	}
	// if not wanted to display on Medium devices
	if( !in_array( 'col-md', get_sub_field('element_visible_on') ) ) {
		array_push($notVisibleOn, 'hidden-md');
		$mdHidden = true;
	}
	// if not wanted to display on Small devices
	if( !in_array( 'col-sm', get_sub_field('element_visible_on') ) ) {
		array_push($notVisibleOn, 'hidden-sm');
		$smHidden = true;
	}
	// if not wanted to display on Xtra small devices
	if( !in_array( 'col-xs', get_sub_field('element_visible_on') ) ) {
		array_push($notVisibleOn, 'hidden-xs');
		$xsHidden = true;
	}

	return $notVisibleOn;

}

function echoBootstrapHidden() {

	$notVisibleOn = bootstapHidden();

	// seporate them out ready to use
	foreach ($notVisibleOn as $hiddenOn) {
	    $hiddenOn = $hiddenOn.' ';
	    echo $hiddenOn;
	}

}