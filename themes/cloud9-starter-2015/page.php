<?php while (have_posts()) : the_post(); ?>
  	<?php
	  if( get_field('show_page_title') == 'false' ) {
	  	// get_template_part('templates/page', 'header');
	  } elseif( get_field('show_page_title') == 'true' ) {
	  	get_template_part('templates/page', 'header');
	  } else {
		  get_template_part('templates/page', 'header');
	  }
	?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
