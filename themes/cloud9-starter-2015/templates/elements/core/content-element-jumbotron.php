<?php
/* ==========================================================================
	Jumbotron
   ========================================================================== */
   use Roots\Sage\Extras;
?>
<div class="jumbotron <?php Extras\echoBootstrapHidden(); ?>">
  <div class="container">
    <?php the_sub_field("jumbotron"); ?>
  </div>
</div>